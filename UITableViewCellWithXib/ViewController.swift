//
//  ViewController.swift
//  UITableViewCellWithXib
//
//  Created by 微向暖年未央 on 2018/2/13.
//  Copyright © 2018年 微向暖年未央. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController ,UITableViewDelegate, UITableViewDataSource{
     //显示频道列表的tableView
    var tableView: UITableView!
    //频道列表数据
    var channels:Array<JSON> = []
    var tableData = [["title":"Swift - 让标签栏按钮UITabBarItem图片居中","image":"img1.png"],
                     ["title":"Swift - 使用SSZipArchive实现文件的压缩、解压缩","image":"img2.png"],
                     ["title":"Swift - 使用LINQ操作数组/集合","image":"img3.png"],
                     ["title":"Swift - 给表格UITableView添加索引功能","image":"img4.png"],
                     ["title":"Swift - 列表项尾部附件点击响应","image":"img5.png"],
                     ["title":"Swift - 自由调整图标按钮中的图标和文字位置","image":"img6.png"]]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView(frame: CGRect(x: 0, y: 20, width: self.view.frame.width, height: self.view.frame.height))
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //设置表格背景色
        self.tableView!.backgroundColor = UIColor(red: 0xf0/255, green: 0xf0/255,
                                                  blue: 0xf0/255, alpha: 1)
        //去掉分隔线
        self.tableView.separatorStyle = .none
        
//        //创建一个重用的单元格
//        self.tableView = UITableView(frame:self.view.frame, style:.plain)
//        self.tableView!.register(UITableViewCell.self,
//                                 forCellReuseIdentifier: "SwiftCell")
        //创建重用的单元格
        self.tableView.register(UINib(nibName: "MYTableViewCell", bundle: nil), forCellReuseIdentifier: "myCell")

        self.view.addSubview(tableView)
       //使用我们的provider进行网络请求（获取频道列表数据）
        DouBanProvider.request(.channels, progress:{
            progress in
            print("当前进度：\(progress.progress)")
        }) { result in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON()
                let json = JSON(data!)
                self.channels = json["channels"].arrayValue
                
                //刷新表格数据
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                }
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }

    //行高
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    //创建各单元显示的内容
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell") as! MYTableViewCell
//        let itemData = tableData[indexPath.row]

        cell.customImage.image = UIImage(named: "diss")
        cell.customLabel.text = channels[indexPath.row]["name"].stringValue


        return cell
    }
    
    //处理列表项的选中事件
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //获取选中项信息
        let channelName = channels[indexPath.row]["name"].stringValue
        let channelId = channels[indexPath.row]["channel_id"].stringValue
        
        //使用我们的provider进行网络请求（根据频道ID获取下面的歌曲）
        DouBanProvider.request(.playlist(channelId)) { result in
            if case let .success(response) = result {
                //解析数据，获取歌曲信息
                let data = try? response.mapJSON()
                let json = JSON(data!)
                let music = json["song"].arrayValue[0]
                let artist = music["artist"].stringValue
                let title = music["title"].stringValue
                let message = "歌手：\(artist)\n歌曲：\(title)"
                
                //将歌曲信息弹出显示
                self.showAlert(title: channelName, message: message)
            }
        }
    }
    
    //显示消息
    func showAlert(title:String, message:String){
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "确定", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

