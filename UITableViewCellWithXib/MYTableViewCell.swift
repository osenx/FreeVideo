//
//  MYTableViewCell.swift
//  UITableViewCellWithXib
//
//  Created by 微向暖年未央 on 2018/2/13.
//  Copyright © 2018年 微向暖年未央. All rights reserved.
//

import UIKit

class MYTableViewCell: UITableViewCell {
  
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var customImage: UIImageView!
    @IBOutlet weak var customLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
